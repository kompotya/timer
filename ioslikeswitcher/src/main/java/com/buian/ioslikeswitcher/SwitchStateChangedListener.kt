package com.buian.ioslikeswitcher

import android.view.View

interface SwitchStateChangedListener {

    fun onStateChanged(view: View, isChecked: Boolean)
}