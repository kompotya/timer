package com.buian.ioslikeswitcher


import android.animation.*
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Point
import android.graphics.RectF
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import androidx.core.content.ContextCompat

private const val DEFAULT_ANIMATION_DURATION = 200
private const val DEFAULT_WIDTH = 50f
private const val DEFAULT_HEIGHT = 30f
private const val BACKGROUND_KEY = "bgColor"
private const val ANIM_X_GAP = "animXGap"

class IosLikeSwitcher : View {

    private var defaultWidth: Float = 0f
    private var defaultHeight: Float = 0f
    private var animTime: Int = 0
    private var closeBgColor: Int = 0
    private var openBgColor: Int = 0
    private var isChecked: Boolean = false
    private var currBgColor: Int = 0
    private var currBorderColor: Int = 0
    private var currSwitcherColor: Int = 0
    private var switcherColorOn: Int = 0
    private var switcherColorOff: Int = 0
    private var paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var borderPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var switcherRadius = 0
    private var switchPadding = 0
    private var switcherCenterPoint = Point()
    private var animXGap = 0
    private var translationTrackBackgroundVh: PropertyValuesHolder? = null
    private var argbVhTrackBackground: PropertyValuesHolder? = null
    private var backGroundRectF: RectF = RectF()
    var offStateBlocked: Boolean = false

    private var trackAnimator: ObjectAnimator? = null
    private var thumbAnimator: ValueAnimator? = null


    var stateChangedListener: SwitchStateChangedListener? = null

    constructor(context: Context?) : super(context) {
        init(null)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    ) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        getContext().obtainStyledAttributes(attrs, R.styleable.IosLikeSwitcher).apply {
            animTime = getInt(R.styleable.IosLikeSwitcher_sbAnimTime, DEFAULT_ANIMATION_DURATION)
            openBgColor = getColor(
                R.styleable.IosLikeSwitcher_sbOpenBgColor,
                ContextCompat.getColor(context, R.color.color_active_green)
            )
            closeBgColor = getColor(
                R.styleable.IosLikeSwitcher_sbCloseBgColor,
                ContextCompat.getColor(context, R.color.color_disable_white)
            )

            currBorderColor = getColor(
                R.styleable.IosLikeSwitcher_sbCloseBgColor,
                ContextCompat.getColor(context, R.color.color_border_bg)
            )

            switcherColorOn = getColor(
                R.styleable.IosLikeSwitcher_sbSwitcherColorOn,
                ContextCompat.getColor(context, R.color.color_disable_white)
            )
            switcherColorOff = getColor(
                R.styleable.IosLikeSwitcher_sbSwitcherColorOff,
                ContextCompat.getColor(context, R.color.color_active_green)
            )
            offStateBlocked = getBoolean(R.styleable.IosLikeSwitcher_sbSwitcherOffBlocked, false)
            isChecked = getString(R.styleable.IosLikeSwitcher_sbSwitchStatus)?.toInt() == 1
            currBgColor = if (isChecked) openBgColor else closeBgColor
            currSwitcherColor = if (isChecked) switcherColorOn else switcherColorOff
            recycle()
        }

        defaultWidth = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, DEFAULT_WIDTH,
            context.resources.displayMetrics
        )

        defaultHeight = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, DEFAULT_HEIGHT,
            context.resources.displayMetrics
        )

        setOnClickListener {
            changeSwitcherStatus()
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthSpecMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSpecSize = MeasureSpec.getSize(widthMeasureSpec)

        val heightSpecMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightSpecSize = MeasureSpec.getSize(heightMeasureSpec)

        var finalWidth = widthSpecSize
        var finalHeight = heightSpecSize

        if (widthSpecMode == MeasureSpec.AT_MOST) {
            finalWidth = defaultWidth.toInt()
        }

        if (heightSpecMode == MeasureSpec.AT_MOST) {
            finalHeight = defaultHeight.toInt()
        }

        if (finalWidth < finalHeight) { //the width must be bigger than height
            finalWidth = finalHeight
        }

        setMeasuredDimension(finalWidth, finalHeight)

        initialize()
    }

    /**
     * initialize the values
     */
    private fun initialize() {
        switchPadding = measuredHeight / 15
        switcherRadius = measuredHeight / 2 - switchPadding

        animXGap = measuredWidth - switchPadding * 2 - switcherRadius * 2

        switcherCenterPoint.x =
            if (isChecked)
                measuredWidth - switchPadding - switcherRadius
            else
                switchPadding + switcherRadius
        switcherCenterPoint.y = switchPadding + switcherRadius
        with(backGroundRectF) {
            left = 0f
            top = 0f
            right = measuredWidth.toFloat()
            bottom = measuredHeight.toFloat()
        }
    }

    override fun onDraw(canvas: Canvas) {
        //draw border round rect
        canvas.drawRoundRect(
            backGroundRectF,
            measuredHeight.toFloat() / 2,
            measuredHeight.toFloat() / 2,
            borderPaint.apply {
                color = currBorderColor
                style = Paint.Style.STROKE
            })
        //draw background round rect
        paint.color = currBgColor
        canvas.drawRoundRect(
            backGroundRectF,
            measuredHeight.toFloat() / 2,
            measuredHeight.toFloat() / 2,
            paint
        )

        //draw switcher
        paint.color = currSwitcherColor
        canvas.drawCircle(
            switcherCenterPoint.x.toFloat(),
            switcherCenterPoint.y.toFloat(),
            switcherRadius.toFloat(), paint
        )
    }

    fun setBgColor(color: Int) {
        currBgColor = color
        invalidate()
    }

    fun getBgColor(): Int {
        return currBgColor
    }

    fun setAnimXGap(xGap: Int) {
        switcherCenterPoint.x = switcherRadius + switchPadding + xGap
    }

    fun getAnimXGap(): Int {
        return switcherCenterPoint.x - switchPadding - switcherRadius
    }

    private fun changeSwitcherStatus() {
        if (trackAnimator?.isRunning == true) {
            return
        }

        if (isChecked && offStateBlocked) {
            stateChangedListener?.onStateChanged(this@IosLikeSwitcher, isChecked)
            return
        }

        argbVhTrackBackground = null
        translationTrackBackgroundVh = null
        if (isChecked) {
            //tracker anim
            argbVhTrackBackground = PropertyValuesHolder.ofInt(BACKGROUND_KEY, openBgColor, closeBgColor)
            argbVhTrackBackground?.setEvaluator(ArgbEvaluator())
            translationTrackBackgroundVh = PropertyValuesHolder.ofInt(ANIM_X_GAP, animXGap, 0)

            //thumb anim
            thumbAnimator = ValueAnimator.ofArgb(switcherColorOn, switcherColorOff).apply {
                duration = animTime.toLong()
            }
        } else {
            //tracker anim
            argbVhTrackBackground = PropertyValuesHolder.ofInt(BACKGROUND_KEY, closeBgColor, openBgColor)
            argbVhTrackBackground?.setEvaluator(ArgbEvaluator())
            translationTrackBackgroundVh = PropertyValuesHolder.ofInt(ANIM_X_GAP, 0, animXGap)
            //thumb anim
            thumbAnimator = ValueAnimator.ofArgb(switcherColorOff, switcherColorOn).apply {
                duration = animTime.toLong()
            }
        }
        if (trackAnimator == null) {
            trackAnimator =
                ObjectAnimator.ofPropertyValuesHolder(this, argbVhTrackBackground, translationTrackBackgroundVh)
                    .setDuration(animTime.toLong())

            trackAnimator?.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    stateChangedListener?.onStateChanged(this@IosLikeSwitcher, isChecked)
                }
            })
        } else {
            trackAnimator?.setValues(argbVhTrackBackground, translationTrackBackgroundVh)
        }

        thumbAnimator?.addUpdateListener {
            currSwitcherColor = it.animatedValue as Int
        }

        trackAnimator?.start()
        thumbAnimator?.start()
        isChecked = !isChecked
    }

    fun changeStatusForce(checked: Boolean) {
        if (trackAnimator?.isRunning == true) {
            return
        }
        argbVhTrackBackground = null
        translationTrackBackgroundVh = null
        if (checked) {
            //tracker anim
            currSwitcherColor = switcherColorOn
            currBgColor = openBgColor
        } else {
            //tracker anim
            currSwitcherColor = switcherColorOff
            currBgColor = closeBgColor
        }
        isChecked = checked
        requestLayout()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        if (trackAnimator?.isRunning == true) {
            trackAnimator?.cancel()
            trackAnimator?.removeAllListeners()

            thumbAnimator?.cancel()
            thumbAnimator?.removeAllUpdateListeners()
        }
    }

    fun isChecked() = isChecked

}