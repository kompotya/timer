package com.buian.timer

import android.app.Application
import android.content.Context


class TimerApp : Application() {

    companion object {
        lateinit var instance: TimerApp
            private set
    }


    override fun onCreate() {
        super.onCreate()
        instance = this
    }

}