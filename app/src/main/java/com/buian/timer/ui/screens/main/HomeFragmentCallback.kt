package com.buian.timer.ui.screens.main

interface HomeFragmentCallback {

    fun onOpenAddItemScreen()

    fun openPetScreen()

    fun openTaskDetails(item: MockItem)
}