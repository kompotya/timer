package com.buian.timer.ui.screens.main

import android.content.Context
import android.os.Bundle
import android.view.View
import com.buian.timer.R
import com.buian.timer.bindInterfaceOrThrow
import com.buian.timer.ui.base.BaseFragment
import com.buian.timer.ui.screens.MainActivity
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TITLE
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TOOLBAR
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment<HomeVM>(), AdapterCallback {

    companion object {
        fun newInstance() = HomeFragment().apply { arguments = Bundle() }
    }

    override val viewModelClass = HomeVM::class.java
    override val layoutId = R.layout.fragment_home
    override fun getScreenTitle() = NO_TITLE
    override fun hasToolbar() = false
    override fun getToolbarId() = NO_TOOLBAR
    private var callback: HomeFragmentCallback? = null

    private val tasksAdapter by lazy {
        context?.let { HomeTasksAdapter(it, this) }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callback = bindInterfaceOrThrow<HomeFragmentCallback>(parentFragment, context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) =
        super.onViewCreated(view, savedInstanceState).also {
            setupUi()
            createMockData()
        }


    override fun observeLiveData(viewModel: HomeVM) {
    }

    override fun onTaskItemClicked(item: MockItem) {
        callback?.openTaskDetails(item)
    }

    override fun onDetach() {
        callback = null
        super.onDetach()
    }

    private fun createMockData() {
        mutableListOf<MockItem>().apply {
            add(MockItemModel(
                getString(R.string.task),
                getString(R.string.task_mock_text),
                getString(R.string.task_mock_time)))
            add(MockItemModel(
                getString(R.string.task),
                getString(R.string.task_mock_text),
                getString(R.string.task_mock_time)))
            add(MockItemModel(
                getString(R.string.task),
                getString(R.string.task_mock_text),
                getString(R.string.task_mock_time)))
        }.let {
            tasksAdapter?.apply {
                addAll(it)
                notifyDataSetChanged()
            }
        }
    }

    private fun setupUi() {
        initTasksRV()
        ivMenu.setOnClickListener {
            (activity as? MainActivity)?.toggleDrawer()
        }
        fabAddItem.setOnClickListener {
            callback?.onOpenAddItemScreen()
        }

        ivFox.setOnClickListener {
            callback?.openPetScreen()
        }
    }

    private fun initTasksRV() {
        with(rvTasks) {
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
            tasksAdapter?.let { adapter = it }
        }
    }
}
