package com.buian.timer.ui.base

import android.app.Application
import com.cleveroad.bootstrap.kotlin_core.ui.BaseLifecycleViewModel

abstract class BaseVM(app: Application) : BaseLifecycleViewModel(app)