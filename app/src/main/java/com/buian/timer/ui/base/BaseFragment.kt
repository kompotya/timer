package com.buian.timer.ui.base

import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import android.view.ViewTreeObserver
import com.buian.timer.BuildConfig
import com.buian.timer.ui.base.dialog.DialogFragmentCallback
import com.cleveroad.bootstrap.kotlin_core.ui.BaseLifecycleFragment
import com.cleveroad.bootstrap.kotlin_core.ui.BaseLifecycleViewModel
import com.cleveroad.bootstrap.kotlin_core.ui.NO_ID
import com.cleveroad.bootstrap.kotlin_ext.withNotNull

abstract class BaseFragment<T : BaseLifecycleViewModel> : BaseLifecycleFragment<T>(),
    DialogFragmentCallback {

    companion object {
        private const val KEYBOARD_VISIBLE_THRESHOLD_DP = 300
    }

    private val keyboardListener = ViewTreeObserver.OnGlobalLayoutListener {
        withNotNull(view) {
            val rect = Rect()
            getWindowVisibleDisplayFrame(rect)
            val heightChangView = rootView.height - (rect.bottom - rect.top)
            when {
                !blockKeyboardListener && heightChangView > KEYBOARD_VISIBLE_THRESHOLD_DP -> {
                    blockKeyboardListener = true
                    onKeyboardSwitch(true)
                }
                blockKeyboardListener && heightChangView <= KEYBOARD_VISIBLE_THRESHOLD_DP -> {
                    blockKeyboardListener = false
                    onKeyboardSwitch(false)
                }
            }
        }
    }
    private var blockKeyboardListener: Boolean = true

    override var endpoint = ""

    override var versionName = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (needKeyboardListener()) view.viewTreeObserver.addOnGlobalLayoutListener(keyboardListener)
    }

    override fun onDestroyView() {
        if (needKeyboardListener()) view?.viewTreeObserver?.removeOnGlobalLayoutListener(keyboardListener)
        super.onDestroyView()
    }

    override fun getVersionsLayoutId() = NO_ID

    override fun getEndPointTextViewId() = NO_ID

    override fun getVersionsTextViewId() = NO_ID

    override fun isDebug() = BuildConfig.DEBUG

    /**
     * Display a warning when going to action back
     */
    override fun showBlockBackAlert() = Unit

    /**
     * Turn on/off keyboard listener
     */
    protected open fun needKeyboardListener() = false

    /**
     * This method is called when keyboard change state
     */
    protected open fun onKeyboardSwitch(isShow: Boolean) = Unit

    override fun onDialogResult(requestCode: Int, resultCode: Int, data: Intent) = Unit

}