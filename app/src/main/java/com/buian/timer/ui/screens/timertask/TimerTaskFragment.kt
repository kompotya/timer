package com.buian.timer.ui.screens.timertask

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.buian.timer.R
import com.buian.timer.bindInterfaceOrThrow
import com.buian.timer.ext.setClickListeners
import com.buian.timer.ext.setVisibility
import com.buian.timer.ui.base.BaseFragment
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TITLE
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TOOLBAR
import kotlinx.android.synthetic.main.fragment_timer_task.*
import org.joda.time.DateTime
import org.joda.time.Duration

class TimerTaskFragment : BaseFragment<TimerTaskVM>(), View.OnClickListener {

    companion object {
        fun newInstance() = TimerTaskFragment().apply { arguments = Bundle() }
    }

    override val viewModelClass = TimerTaskVM::class.java
    override val layoutId = R.layout.fragment_timer_task
    override fun getScreenTitle() = NO_TITLE
    override fun hasToolbar() = false
    override fun getToolbarId() = NO_TOOLBAR
    private var callback: TimerTaskFragmentCallback? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callback = bindInterfaceOrThrow<TimerTaskFragmentCallback>(parentFragment, context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) =
        super.onViewCreated(view, savedInstanceState).also {
            setupUi()
        }


    override fun observeLiveData(viewModel: TimerTaskVM) {
        viewModel.timerLD.observe(this, Observer {
            it?.let {
                avTimer.progress = (it.second.toFloat() / it.first.toFloat())
                tvTimer.text = DateTime().withMillis(it.second).toString("mm:ss")
            }
        })
    }

    override fun onDetach() {
        callback = null
        super.onDetach()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ivStart -> startTimer()
            R.id.ivStop -> {
                viewModel.stopTimer()
                setVisibleActions(false)
            }
            R.id.ivPause -> viewModel.pauseTimer()
            R.id.ivDone -> viewModel.stopTimer()
            R.id.ivBack -> backPressed()
        }
    }

    private fun setupUi() {
        setClickListeners(ivStart, ivStop, ivPause, ivDone, ivBack)
    }

    private fun startTimer() {
        Duration.standardSeconds(30).millis.let {
            viewModel.startTimer(it)
            setVisibleActions(true)
        }
    }

    private fun setVisibleActions(isVisible: Boolean) {
        llActions.setVisibility(isVisible)
        ivStart.setVisibility(isVisible.not())
    }
}
