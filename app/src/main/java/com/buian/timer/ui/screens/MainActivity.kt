package com.buian.timer.ui.screens

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentManager
import com.buian.timer.R
import com.buian.timer.ViewAnimUtils
import com.buian.timer.ext.setVisibility
import com.buian.timer.ui.base.BaseActivity
import com.buian.timer.ui.screens.aboutus.AboutUsFragment
import com.buian.timer.ui.screens.aboutus.AboutUsFragmentCallback
import com.buian.timer.ui.screens.addtask.AddTaskFragment
import com.buian.timer.ui.screens.addtask.AddTaskFragmentCallback
import com.buian.timer.ui.screens.main.HomeFragment
import com.buian.timer.ui.screens.main.HomeFragmentCallback
import com.buian.timer.ui.screens.main.MockItem
import com.buian.timer.ui.screens.petphrase.PetPhraseFragment
import com.buian.timer.ui.screens.timertask.TimerTaskFragment
import com.buian.timer.ui.screens.timertask.TimerTaskFragmentCallback
import com.cleveroad.bootstrap.kotlin_ext.getStringArray
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_menu_layout.*

class MainActivity : BaseActivity<MainVM>(),
    HomeFragmentCallback,
    FragmentManager.OnBackStackChangedListener,
    AboutUsFragmentCallback,
    TimerTaskFragmentCallback,
    AddTaskFragmentCallback {


    companion object {
        private const val ROTATE_ANGLE = 360f
        private const val ROTATE_ANGLE_BACK = 180f
        private const val ROTATE_PIVOT = 0.5f
        private const val ANIMATION_DURATION = 200L


        fun start(context: Context?) {
            context?.apply ctx@{ startActivity(getIntent(this@ctx)) }
        }

        fun getIntent(context: Context?) = Intent(context, MainActivity::class.java)
    }

    override val containerId = R.id.container
    override val layoutId = R.layout.activity_main
    override val viewModelClass = MainVM::class.java

    private var isExpandable = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) openHomeScreen()
        llSettings.setOnClickListener { expandSettings() }
        llFaq.setOnClickListener { openFaqScreen() }
        supportFragmentManager.addOnBackStackChangedListener(this)
        getStringArray(R.array.motivation_phrases)?.let {
            tvMotivatePhrase.text = it.random()
            dlLayout.addDrawerListener(object : DrawerLayout.DrawerListener {
                override fun onDrawerStateChanged(newState: Int) = Unit

                override fun onDrawerSlide(drawerView: View, slideOffset: Float) = Unit

                override fun onDrawerClosed(drawerView: View) {
                    tvMotivatePhrase.text = it.random()
                }

                override fun onDrawerOpened(drawerView: View) = Unit

            })
        }

    }

    override fun observeLiveData(viewModel: MainVM) {
    }

    fun expandSettings() {
        val fromDegrees = if (isExpandable) ROTATE_ANGLE else ROTATE_ANGLE_BACK
        val toDegrees = if (isExpandable) ROTATE_ANGLE_BACK else ROTATE_ANGLE
        showExpandableRating(!isExpandable)
        if (isExpandable) {
            ViewAnimUtils.collapse(llExpandSettings, ANIMATION_DURATION)
        } else {
            ViewAnimUtils.expand(llExpandSettings, ANIMATION_DURATION)
        }
        ivArrow.startAnimation(
            RotateAnimation(
                fromDegrees, toDegrees,
                Animation.RELATIVE_TO_SELF, ROTATE_PIVOT, Animation.RELATIVE_TO_SELF,
                ROTATE_PIVOT
            ).apply {
                duration = ANIMATION_DURATION
                isFillEnabled = true
                fillAfter = true
            })
        isExpandable = !isExpandable
    }

    private fun showExpandableRating(isShow: Boolean) {
        llExpandSettings.setVisibility(!isShow)
    }

    private fun openHomeScreen() {
        replaceFragment(HomeFragment.newInstance(), false)
    }

    private fun openFaqScreen() {
        replaceFragment(AboutUsFragment.newInstance())
    }

    override fun openPetScreen() {
        replaceFragment(PetPhraseFragment.newInstance())
    }

    override fun onOpenAddItemScreen() {
        replaceFragment(AddTaskFragment.newInstance())
    }

    override fun onBackStackChanged() {
        supportFragmentManager.findFragmentById(R.id.container)?.let {
            setDrawerLockMode(
                when (it::class.java) {
                    AboutUsFragment::class.java -> true
                    HomeFragment::class.java -> false
                    AddTaskFragment::class.java -> true
                    PetPhraseFragment::class.java -> true
                    else -> false
                }
            )
        }
    }

    override fun openTaskDetails(item: MockItem) {
        replaceFragment(TimerTaskFragment.newInstance())
    }

    override fun onBackPressed() {
        if (dlLayout.isDrawerOpen(GravityCompat.START)) {
            dlLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    fun toggleDrawer() {
        if (dlLayout.isDrawerOpen(GravityCompat.START)) {
            dlLayout.closeDrawer(GravityCompat.START)
        } else {
            dlLayout.openDrawer(GravityCompat.START)
        }
    }

    private fun setDrawerLockMode(isLocked: Boolean) {
        dlLayout.setDrawerLockMode(if (isLocked) DrawerLayout.LOCK_MODE_LOCKED_CLOSED else DrawerLayout.LOCK_MODE_UNLOCKED)
    }
}
