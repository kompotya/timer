package com.buian.timer.ui.screens.main


interface MockItem {
    val title: String?
    val description: String?
    val timeToComplete: String?
}

class MockItemModel(override val title: String? = null,
                    override val description: String? = null,
                    override val timeToComplete: String? = null) : MockItem