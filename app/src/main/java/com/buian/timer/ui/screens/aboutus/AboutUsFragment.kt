package com.buian.timer.ui.screens.aboutus

import android.content.Context
import android.os.Bundle
import android.view.View
import com.buian.timer.R
import com.buian.timer.bindInterfaceOrThrow
import com.buian.timer.ui.base.BaseFragment
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TITLE
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TOOLBAR
import kotlinx.android.synthetic.main.fragment_about_us.*

class AboutUsFragment : BaseFragment<AboutUsVM>() {

    companion object {
        fun newInstance() = AboutUsFragment().apply { arguments = Bundle() }
    }

    override val viewModelClass = AboutUsVM::class.java
    override val layoutId = R.layout.fragment_about_us
    override fun getScreenTitle() = NO_TITLE
    override fun hasToolbar() = false
    override fun getToolbarId() = NO_TOOLBAR
    private var callback: AboutUsFragmentCallback? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callback = bindInterfaceOrThrow<AboutUsFragmentCallback>(parentFragment, context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) =
        super.onViewCreated(view, savedInstanceState).also {
            setupUi()
        }

    override fun observeLiveData(viewModel: AboutUsVM) {
    }

    override fun onDetach() {
        callback = null
        super.onDetach()
    }

    private fun setupUi() {
        ivBack.setOnClickListener {
            backPressed()
        }
    }
}
