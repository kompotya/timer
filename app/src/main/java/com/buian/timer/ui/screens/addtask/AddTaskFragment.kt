package com.buian.timer.ui.screens.addtask

import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.View
import com.buian.timer.R
import com.buian.timer.bindInterfaceOrThrow
import com.buian.timer.ext.setClickListeners
import com.buian.timer.ui.base.BaseFragment
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TITLE
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TOOLBAR
import kotlinx.android.synthetic.main.fragment_add_task.*
import java.util.*

class AddTaskFragment : BaseFragment<AddTaskVM>(), View.OnClickListener {

    companion object {
        private const val TIME_FORMAT_FOR_PICKER = "%02dч %02dмин"

        fun newInstance() = AddTaskFragment().apply { arguments = Bundle() }
    }

    override val viewModelClass = AddTaskVM::class.java
    override val layoutId = R.layout.fragment_add_task
    override fun getScreenTitle() = NO_TITLE
    override fun hasToolbar() = false
    override fun getToolbarId() = NO_TOOLBAR
    private var callback: AddTaskFragmentCallback? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callback = bindInterfaceOrThrow<AddTaskFragmentCallback>(parentFragment, context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) =
        super.onViewCreated(view, savedInstanceState).also {
            setupUi()
        }

    override fun observeLiveData(viewModel: AddTaskVM) {
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ivBack,
            R.id.ivDone -> backPressed()
            R.id.tvTime -> showTimePicker()
        }
    }

    override fun onDetach() {
        callback = null
        super.onDetach()
    }

    private fun showTimePicker() {
        val calendar = Calendar.getInstance()
        TimePickerDialog(context, R.style.TimePickerTheme, { _, hours, minutes ->
            tvTime.setText(String.format(TIME_FORMAT_FOR_PICKER, hours, minutes))
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true).show()
    }

    private fun setupUi() {
        setClickListeners(ivBack, ivDone, tvTime)
    }
}
