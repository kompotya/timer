package com.buian.timer.ui.screens.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.buian.timer.R
import com.cleveroad.bootstrap.kotlin_core.ui.adapter.BaseRecyclerViewAdapter
import java.lang.ref.WeakReference


class HomeTasksAdapter(
    context: Context,
    itemAdapterCallback: AdapterCallback,
    data: List<MockItem> = listOf()
) :
    BaseRecyclerViewAdapter<MockItem, HomeTasksAdapter.TasksViewHolder>(context, data), AdapterCallback {


    private val weakRefCallback = WeakReference(itemAdapterCallback)

    override fun onBindViewHolder(holder: TasksViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        TasksViewHolder.newInstance(inflater, parent, R.layout.item_task, this)

    override fun onTaskItemClicked(item: MockItem) {
        weakRefCallback.get()?.onTaskItemClicked(item)
    }

    class TasksViewHolder(itemView: View, private val listener: AdapterCallback) : RecyclerView.ViewHolder(itemView) {
        companion object {
            fun newInstance(
                inflater: LayoutInflater, parent: ViewGroup?,
                layout: Int,
                listener: AdapterCallback
            ) = inflater.inflate(layout, parent, false)
                .let { TasksViewHolder(it, listener) }
        }

        private var tvTitle = itemView.findViewById<TextView>(R.id.tvTaskTitle)
        private var tvDescription = itemView.findViewById<TextView>(R.id.tvTaskDescr)
        private var tvTimeToEnd = itemView.findViewById<TextView>(R.id.tvTimeToEnd)
        private var ivStart = itemView.findViewById<ImageView>(R.id.ivStart)

        fun bind(task: MockItem) {
            with(task) {
                tvTitle.text = title
                tvDescription.text = description
                tvTimeToEnd.text = timeToComplete
                ivStart.setOnClickListener { listener.onTaskItemClicked(this) }
            }
        }
    }
}