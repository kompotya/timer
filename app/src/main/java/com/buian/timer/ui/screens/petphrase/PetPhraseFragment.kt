package com.buian.timer.ui.screens.petphrase

import android.os.Bundle
import android.view.View
import com.buian.timer.R
import com.buian.timer.ui.base.BaseFragment
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TITLE
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TOOLBAR
import com.cleveroad.bootstrap.kotlin_ext.getStringArray
import kotlinx.android.synthetic.main.fragment_my_pet.*

class PetPhraseFragment : BaseFragment<PetPhraseVM>() {

    companion object {
        fun newInstance() = PetPhraseFragment().apply { arguments = Bundle() }
    }

    override val viewModelClass = PetPhraseVM::class.java
    override val layoutId = R.layout.fragment_my_pet
    override fun getScreenTitle() = NO_TITLE
    override fun hasToolbar() = false
    override fun getToolbarId() = NO_TOOLBAR

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) =
        super.onViewCreated(view, savedInstanceState).also {
            setupUi()
        }

    override fun observeLiveData(viewModel: PetPhraseVM) = Unit

    private fun setupUi() {
        ivBack.setOnClickListener {
            backPressed()
        }
        context?.getStringArray(R.array.pet_phrases)?.random()?.let {
            tvPetPhrase.text = it
        }
    }
}
