package com.buian.timer.ui.screens.timertask

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.buian.timer.ui.base.BaseVM
import com.cleveroad.bootstrap.kotlin_core.utils.ioToMain
import io.reactivex.Flowable
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

class TimerTaskVM(app: Application) : BaseVM(app) {

    companion object {
        private const val INTERVAL = 100L
    }

    var timerLD = MutableLiveData<Pair<Long, Long>>()

    private var currentTime: Long = 0

    private var timeDisposible: Disposable? = null

    fun startTimer(timeLeft: Long) {
        currentTime = timeLeft
        timeDisposible?.dispose()
        timeDisposible = Flowable.interval(INTERVAL, TimeUnit.MILLISECONDS)
            .compose { ioToMain(it) }
            .subscribe({
                if (currentTime == 0L) timeDisposible?.dispose()
                currentTime -= INTERVAL
                timerLD.postValue(timeLeft to currentTime)
            }, {

            })
    }

    fun stopTimer() {
        timerLD.value = 0L to currentTime
        currentTime = 0
        timeDisposible?.dispose()
    }

    fun pauseTimer() {
        timeDisposible?.dispose()
    }


    fun resumeTimer() {
        startTimer(currentTime)
    }

}
