package com.buian.timer.ui.screens.splash

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.animation.AnticipateOvershootInterpolator
import androidx.constraintlayout.widget.ConstraintSet
import androidx.transition.ChangeBounds
import androidx.transition.TransitionManager
import com.buian.timer.R
import com.buian.timer.preferences.PreferencesProvider
import com.buian.timer.ui.base.BaseActivity
import com.buian.timer.ui.screens.MainActivity
import com.cleveroad.bootstrap.kotlin_core.utils.ioToMainSingle
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_splash.*
import java.util.concurrent.TimeUnit

class SplashActivity : BaseActivity<SplashViewModel>() {

    override val viewModelClass = SplashViewModel::class.java
    override val containerId = R.id.container
    override val layoutId = R.layout.activity_splash

    private var redirectDisposable: Disposable? = null

    companion object {
        private const val SPLASH_DELAY = 1_000L
        private const val ANIMATION_DURATION = 700L
        private const val TENSION = 1.0F

        fun start(context: Context?) {
            context?.apply ctx@{ startActivity(getIntent(this@ctx)) }
        }

        fun getIntent(context: Context?) = Intent(context, SplashActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        redirectToOtherScreen {
            if (PreferencesProvider.firstTimeEnter) {
                showIntro()
            } else {
                MainActivity.start(this)
                finish()
            }
        }

        bStart.setOnClickListener {
            MainActivity.start(this)
            finish()
        }
    }

    private fun showIntro() {
        val constraintSet = ConstraintSet().apply {
            clone(this@SplashActivity, R.layout.activity_splash_clone)
        }
        val transition = ChangeBounds().apply {
            interpolator = AnticipateOvershootInterpolator(TENSION)
            duration = ANIMATION_DURATION
        }
        TransitionManager.beginDelayedTransition(clContainer, transition)
        constraintSet.applyTo(clContainer)
        PreferencesProvider.firstTimeEnter = false
    }

    override fun observeLiveData(viewModel: SplashViewModel) = Unit

    override fun onDestroy() {
        redirectDisposable?.dispose()
        super.onDestroy()
    }

    private fun redirectToOtherScreen(callback: () -> Unit) {
        redirectDisposable = Single.timer(SPLASH_DELAY, TimeUnit.MILLISECONDS)
            .compose(ioToMainSingle())
            .subscribe({
                callback()
            }, {
                Log.e("LOG", it.localizedMessage)
            })
    }

}
