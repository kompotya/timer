package com.buian.timer.ui.base


class RequestCodes {
    companion object {
        private var currentRequestCode = 0
    }

    enum class RequestCode {
        REQUEST_WRITE_EXTERNAL_STORAGE,
        REQUEST_PICK_PHOTO_CAMERA,
        REQUEST_PICK_PHOTO_GALLERY,
        REQUEST_CHOOSE_COUNTRY;

        private val value = ++currentRequestCode

        operator fun invoke() = value
    }
}