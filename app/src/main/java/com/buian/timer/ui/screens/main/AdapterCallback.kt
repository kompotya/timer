package com.buian.timer.ui.screens.main

interface AdapterCallback {

    fun onTaskItemClicked(item: MockItem)
}