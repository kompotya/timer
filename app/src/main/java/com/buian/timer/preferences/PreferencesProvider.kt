package com.buian.timer.preferences

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import com.buian.timer.TimerApp

@SuppressLint("CommitPrefEdits", "ApplySharedPref")
internal object PreferencesProvider {
    private const val PREFS_NAME: String = "TimerPrefs"
    private const val FIRST_TIME_ENTER: String = "FIRST_TIME_ENTER"

    private val preferences: SharedPreferences = TimerApp.instance
        .applicationContext
        .getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)


    var firstTimeEnter: Boolean
        get() = preferences.getBoolean(FIRST_TIME_ENTER, true)
        set(value) {
            preferences.edit()
                .putBoolean(FIRST_TIME_ENTER, value)
                .commit()
        }
}