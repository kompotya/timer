package com.buian.timer

import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Transformation
import com.buian.timer.ext.hide
import com.buian.timer.ext.show

object ViewAnimUtils {

    private const val ZERO_VALUE = 0
    private const val DEFAULT_INTERPOLATED_TIME = 1f

    fun expand(view: View, animationDuration: Long) {
        with(view) {
            measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            val targetHeight = measuredHeight.toLong()
            layoutParams.height = ZERO_VALUE
            show()
            val animation = object : Animation() {
                override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                    layoutParams.height = if (interpolatedTime == DEFAULT_INTERPOLATED_TIME) {
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    } else {
                        (targetHeight * interpolatedTime).toInt()
                    }
                    requestLayout()
                }

                override fun willChangeBounds(): Boolean {
                    return true
                }
            }
            animation.duration = animationDuration
            startAnimation(animation)
        }
    }

    fun collapse(view: View, animationDuration: Long) {
        with(view) {
            val initialHeight = measuredHeight
            val animation = object : Animation() {

                override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                    if (interpolatedTime == DEFAULT_INTERPOLATED_TIME) {
                        hide()
                    } else {
                        layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                        requestLayout()
                    }
                }

                override fun willChangeBounds(): Boolean {
                    return true
                }
            }
            animation.duration = animationDuration
            startAnimation(animation)
        }
    }
}